http://www.lightfor.org/snaker/index.html
概述
Snaker是一个基于Java的开源工作流引擎，适用于企业应用中常见的业务流程。本着轻量、简单、灵巧理念设计，定位于简单集成，多环境支持


轻量:核心代码行数大约7000行，强大的扩展性，支持Spring、Jfinal、Nutz平台级框架；支持Jdbc、SpringJdbc、Hibernate3or4、Mybatis等orm框架

简单:表设计简单，流程组件简单[start/end/task/custom/subprocess/decision/fork/join]

灵巧:暴露大量可扩展接口，支持流程设计器、流程引擎的组件模型自定义[节点自定义、属性自定义、表单自定义]

开源协议:Apache License Version 2.0

依赖
maven项目中，直接在pom文件中添加snaker的依赖即可:

1
2
3
4
5
6
<dependency>
    <groupId>com.github.snakerflow</groupId>
    <artifactId>snaker-core</artifactId>
    <version>2.5.1</version>
</dependency>
github
流程引擎:
https://github.com/snakerflow/snakerflow

Eclipse插件:
https://github.com/snakerflow/snaker-designer

QQ群:293568574(已满)  201639096(2群)

百度网盘：http://pan.baidu.com/s/1hqza8BU


oscgit
流程引擎:
http://git.oschina.net/yuqs/snakerflow

Eclipse插件:
http://git.oschina.net/yuqs/snaker-designer

邮箱:snakerflow@163.com

版本升级日志
version 2.5.1[最新版]
-ADD:hibernate4子模块实现getConnection的api
-BUG FIX:修复getActiveOrders查询[流程中文名称检索时]
-BUG FIX:修复complete完成实例[hibernate作为ORM时，会造成session重复对象]